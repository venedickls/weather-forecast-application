package com.example.forecast.data.network.response

import com.example.forecast.data.db.entity.WeatherLocation
import com.google.gson.annotations.SerializedName

data class FutureWeatherResponse(
    val location: WeatherLocation,
    @SerializedName("forecast")
    val futureWeatherEntries: ForecastDaysContainer
)